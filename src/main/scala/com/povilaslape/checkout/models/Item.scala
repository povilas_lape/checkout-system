package com.povilaslape.checkout.models

sealed trait Item { def price: BigDecimal }

case object Apple extends Item { override val price = 60 }

case object Orange extends Item { override val price = 25 }
