package com.povilaslape.checkout

import com.povilaslape.checkout.models.{Apple, Item, Orange}

object CheckoutSystem {

  val noPromo: (Int, BigDecimal) => BigDecimal =
    (count: Int, price: BigDecimal) => count * price

  val twoForOnePromo: (Int, BigDecimal) => BigDecimal =
    (count: Int, price: BigDecimal) => (count / 2) * price + (count % 2) * price

  val threeForTwoPromo: (Int, BigDecimal) => BigDecimal = {
    (count: Int, price: BigDecimal) => (count / 3 * 2) * price + (count % 3) * price
  }

  def checkout(items: List[Item],
               twoForOnePromo: (Int, BigDecimal) => BigDecimal = noPromo,
               threeForTwoPromo: (Int, BigDecimal) => BigDecimal = noPromo): BigDecimal = {
    val appleCount = items.count(_ == Apple)
    val orangeCount = items.count(_ == Orange)

    val applePrice = twoForOnePromo(appleCount, Apple.price)
    val orangePrice = threeForTwoPromo(orangeCount, Orange.price)

    applePrice + orangePrice
  }

}
