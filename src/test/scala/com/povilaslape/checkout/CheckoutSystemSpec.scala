package com.povilaslape.checkout

import com.povilaslape.checkout.models.{Apple, Item, Orange}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class CheckoutSystemSpec extends AnyWordSpec with Matchers {

  "Checkout System" should {

    "return 0 if shopping items list is empty" in {
      val result = CheckoutSystem.checkout(List.empty[Item])

      result shouldBe 0
    }

    "calculate total price when basket contains only Apples" in {
      val apples = List.fill(3)(Apple)
      val result = CheckoutSystem.checkout(apples)

      result shouldBe 180
    }

    "calculate total price when basket contains only Oranges" in {
      val oranges = List.fill(3)(Orange)
      val result = CheckoutSystem.checkout(oranges)

      result shouldBe 75
    }

    "calculate total price when basket contains Apples and Oranges" in {
      val result = CheckoutSystem.checkout(List(Apple, Apple, Orange, Apple))

      result shouldBe 205
    }

    "calculate total price taking into account Apples 2 for 1 offer (two apples)" in {
      val result = CheckoutSystem.checkout(List(Apple, Apple), CheckoutSystem.twoForOnePromo)

      result shouldBe 60
    }

    "calculate total price taking into account Apples 2 for 1 offer" in {
      val result = CheckoutSystem.checkout(List(Apple, Apple, Orange, Apple), CheckoutSystem.twoForOnePromo)

      result shouldBe 145
    }

    "calculate total price taking into account Oranges 3 for 2 offer (three oranges)" in {
      val result = CheckoutSystem.checkout(
        List(Orange, Orange, Orange),
        threeForTwoPromo = CheckoutSystem.threeForTwoPromo
      )

      result shouldBe 50
    }

    "calculate total price taking into account Oranges 3 for 2 offer (six oranges)" in {
      val oranges = List.fill(6)(Orange)

      val result = CheckoutSystem.checkout(
        oranges,
        threeForTwoPromo = CheckoutSystem.threeForTwoPromo
      )

      result shouldBe 100
    }

    "calculate total price taking into account Apples 2 for 1 offer and Oranges 3 for 2 offer" in {
      val result = CheckoutSystem.checkout(
        List(Apple, Apple, Orange, Orange, Orange, Orange, Apple),
        CheckoutSystem.twoForOnePromo,
        CheckoutSystem.threeForTwoPromo
      )

      result shouldBe 195
    }

  }

}
