name := "checkout-system"

version := "0.1"

scalaVersion := "2.13.1"

organization := "com.povilaslape"

libraryDependencies ++= Seq(
  "org.scalatest"  %% "scalatest"  % "3.1.0"  %  Test
)